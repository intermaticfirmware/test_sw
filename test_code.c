//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: test_code.c contains software for testing the board
//               hardware functions.
//
//-----------------------------------------------------------------------------
#include "test_code.h"
#include "hal-config-sloan.h"
#include "em_gpio.h"
#include "em_adc.h"
#include "uart.h"
#include "pwm.h"
#include "adc.h"
#include "debug.h"

#define INPUT_HIGH (1)

volatile uint32_t sample;
volatile uint32_t supply_voltage;

//--------------------------------------------------------------------------------
// NAME      : test_code displays status of jumpers, motion sensor, and
//             the current adc reading.
// ABSTRACT  :
// ARGUMENTS :
//   none
// RETURN    :
//   none
//--------------------------------------------------------------------------------
void test_code(void)
{
	uint8_t test_string[10];
	int32_t pwm_incr = 0;
	uint32_t motion;
	uint32_t led_state;

    // read each jumper and store its status
	if (GPIO_PinInGet(GPIO_PORT, ZONE_BIT4) == INPUT_HIGH)
	{
		test_string[0] = 'H';
	}
	else
	{
		test_string[0] = 'L';
		pwm_incr = pwm_incr + 20;
	}

	if (GPIO_PinInGet(GPIO_PORT, ZONE_BIT3) == INPUT_HIGH)
	{
		test_string[1] = 'H';
	}
	else
	{
		test_string[1] = 'L';
		pwm_incr = pwm_incr + 20;
	}

	if (GPIO_PinInGet(GPIO_PORT, ZONE_BIT2) == INPUT_HIGH)
	{
		test_string[2] = 'H';
	}
	else
	{
		test_string[2] = 'L';
		pwm_incr = pwm_incr + 20;
	}

	if (GPIO_PinInGet(GPIO_PORT, ZONE_BIT1) == INPUT_HIGH)
	{
		test_string[3] = 'H';
	}
	else
	{
		test_string[3] = 'L';
		pwm_incr = pwm_incr + 20;
	}

	if (GPIO_PinInGet(GPIO_PORT, ZONE_BIT0) == INPUT_HIGH)
	{
		test_string[4] = 'H';
	}
	else
	{
		test_string[4] = 'L';
		pwm_incr = pwm_incr + 20;
	}

	if (GPIO_PinInGet(SW_PORT, SW0) == INPUT_HIGH)
	{
		test_string[5] = 'H';
	}
	else
	{
		test_string[5] = 'L';
	}

	if (GPIO_PinInGet(SW_PORT, SW1) == INPUT_HIGH)
	{
		test_string[6] = 'H';
	}
	else
	{
		test_string[6] = 'L';
	}

	test_string[7] = ' ';
	test_string[8] = '\0';

    // Get ADC result
    sample = get_adc_value(CURRENT_ADC_CHAN);

    supply_voltage = get_adc_value(CURRENT_ADC_VDD);

    led_state = GPIO_PinOutGet(LED0_PORT, LED0_PIN);

    motion = GPIO_PinInGet(SW_PORT, MOTION_IN);

    DEBUGV(DTST,"%s motion %ld led %ld adc raw %4lx \r\n", test_string, motion, led_state, sample)

	set_duty_cycle(pwm_incr);

}


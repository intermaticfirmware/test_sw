//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: hal-config-sloan.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef HAL_CONFIG_SLOAN_H
#define HAL_CONFIG_SLOAN_H

// PWM Output Pin Configuration
#define PWM_PORT										(gpioPortF)

#define LED0_PORT										(gpioPortF)
#define PWMOUT_PIN              						(6U)
#define LED0_PIN										(5U)

#define SW0												(14U)
#define SW1												(15U)

#define SW_PORT											(gpioPortD)

#define MOTION_IN										(13U)

#define ZONE_BIT0                                       (1U)
#define ZONE_BIT1										(2U)
#define ZONE_BIT2										(3U)
#define ZONE_BIT3										(4U)
#define ZONE_BIT4										(5U)

#define GPIO_PORT										(gpioPortA)


#define CLI_TXD											(10U)
#define CLI_RXD											(11U)

#define UART_PORT										(gpioPortC)



#endif /* HAL_CONFIG_SLOAN_H */

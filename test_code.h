//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: test_code.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef TEST_CODE_H_
#define TEST_CODE_H_

void test_code(void);

#endif /* TEST_CODE_H_ */

//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: pwm.h contains constants, macros, structure definitions,
//               and public prototypes owned by the Screen Library module.
//
//-----------------------------------------------------------------------------
#ifndef PWM_H_
#define PWM_H_

void pwm_init(void);
void set_duty_cycle(int dutyCycle);


#endif /* PWM_H_ */

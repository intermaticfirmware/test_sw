//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: uart.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef UART_H
#define UART_H

#define TXBUFFSIZE 2000

typedef struct {
	uint8_t tx_buf[TXBUFFSIZE];
	int32_t tx_head_point;
	int32_t tx_tail_point;
	int32_t tx_count;
}UART_BUFFER_T;


void  uart_init(UART_BUFFER_T *p_Uart_Buffer);
void  uart_debug(const char *fmt, ...);
int32_t uart_put_string(uint8_t *buffer, int32_t num_char);

#endif /* UART_H */

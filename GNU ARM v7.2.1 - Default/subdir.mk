################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../adc.c \
../dcd.c \
../gatt_db.c \
../init_app.c \
../init_board.c \
../init_mcu.c \
../main.c \
../pti.c \
../pwm.c \
../test_code.c \
../uart.c 

OBJS += \
./adc.o \
./dcd.o \
./gatt_db.o \
./init_app.o \
./init_board.o \
./init_mcu.o \
./main.o \
./pti.o \
./pwm.o \
./test_code.o \
./uart.o 

C_DEPS += \
./adc.d \
./dcd.d \
./gatt_db.d \
./init_app.d \
./init_board.d \
./init_mcu.d \
./main.d \
./pti.d \
./pwm.d \
./test_code.d \
./uart.d 


# Each subdirectory must supply rules for building sources it contributes
adc.o: ../adc.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"adc.d" -MT"adc.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

dcd.o: ../dcd.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"dcd.d" -MT"dcd.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

gatt_db.o: ../gatt_db.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"gatt_db.d" -MT"gatt_db.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

init_app.o: ../init_app.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"init_app.d" -MT"init_app.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

init_board.o: ../init_board.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"init_board.d" -MT"init_board.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

init_mcu.o: ../init_mcu.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"init_mcu.d" -MT"init_mcu.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

main.o: ../main.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"main.d" -MT"main.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

pti.o: ../pti.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"pti.d" -MT"pti.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

pwm.o: ../pwm.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"pwm.d" -MT"pwm.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

test_code.o: ../test_code.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"test_code.d" -MT"test_code.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

uart.o: ../uart.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"uart.d" -MT"uart.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



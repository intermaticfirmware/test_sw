################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/middleware/glib/dmd/display/dmd_display.c 

OBJS += \
./platform/middleware/glib/dmd/display/dmd_display.o 

C_DEPS += \
./platform/middleware/glib/dmd/display/dmd_display.d 


# Each subdirectory must supply rules for building sources it contributes
platform/middleware/glib/dmd/display/dmd_display.o: ../platform/middleware/glib/dmd/display/dmd_display.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DEFR32BG13P632F512GM48=1' '-DMESH_LIB_NATIVE=1' '-D__HEAP_SIZE=0x1200' '-D__STACK_SIZE=0x1000' '-DHAL_CONFIG=1' -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\soc" -I"C:\git\test_sw\platform\halconfig\inc\hal-config" -I"C:\git\test_sw\platform\emdrv\uartdrv\inc" -I"C:\git\test_sw\platform\bootloader\api" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\src" -I"C:\git\test_sw\hardware\kit\common\drivers" -I"C:\git\test_sw\platform\radio\rail_lib\common" -I"C:\git\test_sw\platform\emdrv\sleep\inc" -I"C:\git\test_sw" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source\GCC" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc" -I"C:\git\test_sw\platform\emdrv\common\inc" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\inc\common" -I"C:\git\test_sw\platform\middleware\glib\dmd" -I"C:\git\test_sw\platform\emdrv\sleep\src" -I"C:\git\test_sw\platform\emlib\src" -I"C:\git\test_sw\protocol\bluetooth\bt_mesh\src" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Include" -I"C:\git\test_sw\platform\middleware\glib" -I"C:\git\test_sw\platform\radio\rail_lib\chip\efr32" -I"C:\git\test_sw\platform\middleware\glib\dmd\display" -I"C:\git\test_sw\platform\emlib\inc" -I"C:\git\test_sw\platform\CMSIS\Include" -I"C:\git\test_sw\hardware\kit\EFR32BG13_BRD4104A\config" -I"C:\git\test_sw\platform\Device\SiliconLabs\EFR32BG13P\Source" -I"C:\git\test_sw\platform\middleware\glib\dmd\ssd2119" -I"C:\git\test_sw\platform\emdrv\gpiointerrupt\inc" -I"C:\git\test_sw\hardware\kit\common\bsp" -I"C:\git\test_sw\platform\middleware\glib\glib" -I"C:\git\test_sw\hardware\kit\common\halconfig" -O0 -fno-builtin -flto -Wall -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"platform/middleware/glib/dmd/display/dmd_display.d" -MT"platform/middleware/glib/dmd/display/dmd_display.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


